import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { HighlightsPage } from "./highlights";

@NgModule({
  declarations: [
    //ShowsPage,
  ],
  imports: [IonicPageModule.forChild(HighlightsPage)]
})
export class HighlightsPageModule {}
