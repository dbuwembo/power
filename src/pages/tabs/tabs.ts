import { NewsPage } from './../news/news';
import { Component , ViewChild} from '@angular/core';
import {  Nav, Platform, NavParams } from 'ionic-angular';
import { NavController } from 'ionic-angular';


import { HomePage } from '../home/home';
import { MatchesPage } from '../matches/matches';
import { FixturesPage } from '../fixtures/fixtures';
import { HighlightsPage } from '../highlights/highlights';
import { LeaguetPage } from '../leaguet/leaguet';



@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = NewsPage;
  tab3Root = HighlightsPage;
  tab4Root = FixturesPage;
  tab5Root = LeaguetPage;

  constructor(navParams: NavParams ) {
   // set the default index to 0, the first tab
   
  }

}
