import { Component } from "@angular/core";
import { NavController , MenuController} from "ionic-angular";
import { ItemsProvider } from "../../providers/webitems/webitems";
import { LoadingController } from "ionic-angular/components/loading/loading-controller";
import { NewsDetailsPage } from "../news-details/news-details";


import "rxjs/add/operator/mergeMap";
import * as $ from "jquery";
import 'slick-carousel/slick/slick';

import {Http, Headers, RequestOptions}  from "@angular/http";
import {Storage} from '@ionic/storage';

import {
  InAppBrowser,
  InAppBrowserOptions
} from "@ionic-native/in-app-browser";
import { PopoverController } from "ionic-angular";

@Component({
  selector: "page-home", 
  templateUrl: "home.html"
})
export class HomePage {
 
  isLoading: boolean;
  currentPage = 0;
  connectionFailed = false;
  latestdate: any
        lhomeClub: any
        lhomeresult: any
        lhomeImg: any 
        lawayClub: any 
        lawayresult: any 
        lawayImg: any 
        resultsi: any
        trophies: any
        resultsicon: any 
        gamesicon: any
        natestdate: any 
        nhomeClub: any 
        nhomeresult: any 
        nhomeImg: any
        nawayClub: any 
        nawayresult: any
        nawayImg: any 

        unatestdate: any 
        unhomeClub: any 
        unhomeresult: any 
        unhomeImg: any
        unawayClub: any 
        unawayresult: any
        nresults: any
        counter: any
  myslides: any
  myclubs: any
  myteam: any
  news: any
  items = [];
  node:  string[];
  newItems:any;
  event:any;
  ngame:any;
  loggedInUser:any;
  loggedInNumber:any;
  loggedInEmail:any;
  newsTitle:any;


  loading = this.loadingCtrl.create({
  spinner: 'hide',
  cssClass: 'my-loading-class',
    content: `
      <div class="custom-spinner-container">
        <div class="custom-spinner-box ">
           <img class="animated bounceIn" src="assets/imgs/icon.png" />
        </div>
        <div class="overlay"></div>
      </div>`,
    duration: 5000
  });
 


  constructor(
    public navCtrl: NavController,
    private _itemsSvc: ItemsProvider,
    public loadingCtrl: LoadingController,
    private inAppBrowser: InAppBrowser,
    public popoverCtrl: PopoverController,
    private menuCtrl: MenuController,
    private http: Http,
    private storage: Storage
  ) {this.menuCtrl.enable(true);}



  ionViewDidLoad() {

    
    this.loading.present();
    this.refresh();

    this.storage.get('authenticatedUser').then(res=>{
      console.log(res)
        
        
     this.loggedInUser = res?res.name:''; 
     this.loggedInNumber = res?res.phonenumber:'';
     this.loggedInEmail = res?res.email:'';
     this.newsTitle = 'Accessed Home';
      var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let data = {
        name: this.loggedInUser,
        data: this.newsTitle,
        mobile: this.loggedInNumber,
        clubid: 'power',
        type: 'HomePage'
      };
    this.http.post('http://bet-ways.com/appdata/audit.php',data, options)
    .map(res => res.json())
    .subscribe(res => {
     console.log(res); 

    });
      
    });
    
    
  }



  getLatestResults() {

    this._itemsSvc.getLatestResults().subscribe(
      res => {
        this.isLoading = false;
        this.nresults = res.nodes;
              setTimeout(() => {
                $('.myCarouselr').slick({
            dots: false,
            centerMode: false,
            infinite: true,
            centerPadding: '10px',
            slidesToShow: 1,
            arrows:true,
            autoplay:true
          });
              }, 5500);
              //console.log(this.myslides);
              
              this.loading.dismiss();

    
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );
  }
   


  
  getSlides(){
    
    this._itemsSvc.getSlides().subscribe(
      res => {
        this.isLoading = false;
        this.myslides = res.nodes;
        setTimeout(() => {
            $('.myCarousels').slick({
            dots: false,
            centerMode: false,
            infinite: true,
            centerPadding: '0',
            slidesToShow: 1,
            arrows:false,
            autoplay:true,
            autoplaySpeed: '3000',
            speed: '1200',
            fade:true
          });
          }, 1500);
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );

  }


   

 
 

  refresh() {
    this.getLatestResults();
    this.getSlides();

  }

 

  openPage(data) {
    this.navCtrl.push(NewsDetailsPage, { details: data });
  }

  openUrl(url: string) {
    const options: InAppBrowserOptions = {
      zoom: "no"
    };
    // Opening a URL and returning an InAppBrowserObject
    this.inAppBrowser.create(url, "_self", options);
  }


}
