import { MatchDetailsPage } from './../match-details/match-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemsProvider } from "../../providers/webitems/webitems";
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

import 'rxjs/add/operator/map';
import {Http, Headers, RequestOptions}  from "@angular/http";
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-fixtures',
  templateUrl: 'fixtures.html',
})
export class FixturesPage {
  matches: any;
  isLoading: boolean;
  currentPage = 0;
  connectionFailed = false;
  newItems: any
  errorMessage: string;
  page = 0;
  toggled: boolean;
    loggedInUser:any;
  loggedInNumber:any;
  loggedInEmail:any;
  newsTitle:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
   private _itemsSvc: ItemsProvider, 
   public loadingCtrl: LoadingController,
    private http: Http,
    private storage: Storage) {
  }

  ionViewDidLoad() {
    this.getAllFixtures();
      this.storage.get('authenticatedUser').then(res=>{
      console.log(res)
        
        
     this.loggedInUser = res?res.name:''; 
     this.loggedInNumber = res?res.phonenumber:'';
     this.loggedInEmail = res?res.email:'';
     this.newsTitle = 'Accessed Fixtures';
      var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let data = {
        name: this.loggedInUser,
        data: this.newsTitle,
        mobile: this.loggedInNumber,
        clubid: 'power',
        type: 'FixturesPage'
      };
    this.http.post('http://bet-ways.com/appdata/audit.php',data, options)
    .map(res => res.json())
    .subscribe(res => {
     console.log(res); 

    });
      
    });
  }

  getAllFixtures() {
    this.isLoading = true;
    let loader = this.loadingCtrl.create({
    spinner: 'hide',
    cssClass: 'my-loading-class',
    content: `
      <div class="custom-spinner-container">
        <div class="custom-spinner-box ">
           <img class="animated bounceIn" src="assets/imgs/icon.png" />
        </div>
        <div class="overlay"></div>
      </div>`,
    duration: 5000
    });
    loader.present();
    this._itemsSvc.getAllFixtures(this.currentPage).subscribe(res => {
      this.isLoading = false;
      this.matches = res.nodes;
      loader.dismiss();
    }, err => {
      this.isLoading = false;
      if (err.name === 'HttpErrorResponse') {
        this.connectionFailed = true;
      }
      console.log(err.name);
      loader.dismiss();
    });
  }

  openPage(data) {
    console.log(data);
    this.navCtrl.push(MatchDetailsPage, { details: data })
  }

  doRefresh(refresher) {
    this._itemsSvc.getAllFixtures(this.currentPage).subscribe(res => {
      this.isLoading = false;
      this.matches = res.nodes;
      refresher.complete();
    }, err => {
      this.isLoading = false;
      refresher.complete();
      if (err.name === 'HttpErrorResponse') {
        this.connectionFailed = true;
      }
      console.log(err.name);
    });
  }

    //load all items if end is reached
    doInfinite(infiniteScroll) {
    this.page = this.page+1;
    setTimeout(() => {
      this._itemsSvc.getAllFixtures(this.page)
         .subscribe(
           res => {
             this.newItems = res.nodes;
             for(let i=0; i<this.newItems.length; i++) {
               this.matches.push(this.newItems[i]);
             }
           },
           error =>  this.errorMessage = <any>error);

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1000);
  }

}
