import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController , MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators,  } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { LoadingController } from 'ionic-angular';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { EmailValidator } from '@angular/forms';


import {Http, Headers, RequestOptions}  from "@angular/http";
import 'rxjs/add/operator/map';
import * as firebase from 'firebase/app';



/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  public recaptchaVerifier:firebase.auth.RecaptchaVerifier;


  registerUserForm: FormGroup;
  loginError: string;
  alert_title = ''
  response: any;

  //@ViewChild("email") email;
  @ViewChild("fname") fname;
  @ViewChild("mobilenumber") mobilenumber;
  @ViewChild("password") password;
  @ViewChild("clubid") clubid;
  
  
  constructor(public navCtrl: NavController, public navParams: NavParams,fb: FormBuilder, 
    private auth: AuthProvider, private http: Http, 
    public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    private menuCtrl: MenuController) {

      this.registerUserForm = fb.group({
        fname: ['', Validators.compose([Validators.required])],
        //email: ['',  Validators.compose([Validators.required])],
        mobilenumber: ["", [Validators.required]],
        clubid: ["", [Validators.required]],
        password: ['', Validators.compose([Validators.required])]
      });
      this.menuCtrl.enable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

  }

   registerUser(){
 //// check to confirm the username, email, telephone and password fields are filled

  if(this.fname.value=="" ){

 let alert = this.alertCtrl.create({

 title:"ATTENTION",
 subTitle:"Your Name field is empty",
 buttons: ['OK']
 });

 alert.present();
  } 
 else 
  if(this.mobilenumber.value=="" ){

 let alert = this.alertCtrl.create({

 title:"ATTENTION",
 subTitle:"Mobile number field is empty",
 buttons: ['OK']
 });

 alert.present();
  } else
 if(this.password.value==""){

 let alert = this.alertCtrl.create({

 title:"ATTENTION",
 subTitle:"Password field is empty",
 buttons: ['OK']
 });

 alert.present();
      
}
 else 
 {   

      let datatosend = {
                      username: this.fname.value,
                      password: this.password.value,
                      mobile: this.mobilenumber.value,
                      clubid: this.clubid.value,
                  };
      var headers = new Headers();
                  headers.append("Accept", 'application/json');
                  headers.append('Content-Type', 'application/json' );
      let options = new RequestOptions({ headers: headers });

      let loader = this.loadingCtrl.create({
                    content: 'Processing please wait...',
                  });

      const appVerifier = this.recaptchaVerifier;
      const phoneNumberString = "+256" + this.mobilenumber.value;
      firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
      .then( confirmationResult => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        let prompt = this.alertCtrl.create({
        title: 'Enter the Confirmation code',
        inputs: [{ name: 'confirmationCode', placeholder: 'Confirmation Code' }],
        buttons: [
          { text: 'Cancel',
            handler: data => { console.log('Cancel clicked'); }
          },
          { text: 'Send',
            handler: data => {
              confirmationResult.confirm(data.confirmationCode)
                .then((result) => {
                  // User signed in successfully.
                  console.log(result.user);        
                  

                 if(result.user){
                  
                loader.present().then(() => {
                this.http.post('http://bet-ways.com/appdata/signin.php',datatosend, options)
                .map(res => res.json())
                .subscribe(res => {

                 loader.dismiss()
                 if(res=="Yes"){
                  let alert = this.alertCtrl.create({
                    title:"ERROR",
                    subTitle:(res),
                    buttons: ['OK']
                    });
                   
                    alert.present();
                 

                   }
                   else if(res=="Registration successfull"){
                  let alert = this.alertCtrl.create({
                    title:"CONGRATS",
                    subTitle:(res),
                    buttons: ['OK']
                    });
                   
                    alert.present();
                 this.navCtrl.setRoot(LoginPage);

                  }else
                  {
                 let alert = this.alertCtrl.create({
                 title:"ERROR",
                 subTitle:(res),
                 buttons: ['OK']
                 });

                 alert.present();
                  } 
                });
                });

                 }
                
                  // ...
                }).catch((error)=> {
                  // User couldn't sign in (bad verification code?)
                  console.log(error)
                  let alert = this.alertCtrl.create({
                        message: error.message,
                        buttons: ['OK']
                      });
                      alert.present();
                      this.navCtrl.setRoot(LoginPage);
                  
                });
            }
          }
        ]
      });
      prompt.present();
    })
    .catch(function (error) {
      console.error("SMS not sent", error);
    });

 }

}

}
