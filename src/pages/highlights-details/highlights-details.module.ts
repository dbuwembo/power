import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HighlightsDetailsPage } from './highlights-details';

@NgModule({
  declarations: [
    //NewsDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(HighlightsDetailsPage),
  ],
})
export class HighlightsDetailsPageModule {}
