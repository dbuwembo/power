import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

import 'rxjs/add/operator/map';
import {Http, Headers, RequestOptions}  from "@angular/http";
import {Storage} from '@ionic/storage';
@IonicPage()
@Component({
  selector: "page-highlights-details",
  templateUrl: "highlights-details.html"
})
export class HighlightsDetailsPage {
  video: any;
  title = "OFFLINE!";
  vdate: any;
  newsImg: any;
  loggedInUser:any;
  loggedInNumber:any;
  loggedInEmail:any;
  newsTitle:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private http: Http,
    private storage: Storage
  ) {}

  ionViewDidLoad() {
    this.storage.get('authenticatedUser').then(res=>{
      console.log(res)
        
        
     this.loggedInUser = res?res.name:''; 
     this.loggedInNumber = res?res.phonenumber:'';
     this.loggedInEmail = res?res.email:'';
     this.newsTitle = this.navParams.data.details['node'].title;
      var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let data = {
        name: this.loggedInUser,
        data: this.newsTitle,
        mobile: this.loggedInNumber,
        clubid: 'power',
        type: 'Highlights'
      };
    this.http.post('http://bet-ways.com/appdata/audit.php',data, options)
    .map(res => res.json())
    .subscribe(res => {
     console.log(res); 

    });
      
    });
    this.title = this.navParams.data.details['node'].title;
    this.video = this.navParams.data.details['node'].field_video_link_video_url;
    this.vdate = this.navParams.data.details['node'].field_on;
  
  }


}
