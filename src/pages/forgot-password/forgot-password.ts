import { Component , ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';

import {Http, Headers, RequestOptions}  from "@angular/http";
import 'rxjs/add/operator/map';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  forgotPasswordForm: FormGroup;
  loginError: string;
  alert_title = ''
   @ViewChild("mobilenumber") mobilenumber;
  @ViewChild("password") password;
  @ViewChild("clubid") clubid;

  constructor(public navCtrl: NavController, public navParams: NavParams,fb: FormBuilder, 
    public loadingCtrl: LoadingController,
     private http: Http,   private auth: AuthProvider, 
     public alertCtrl: AlertController,
    private menuCtrl: MenuController) {

    this.forgotPasswordForm = fb.group({
      mobilenumber: ['', [Validators.required]],
      clubid: ["", [Validators.required]],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]

    });
    this.menuCtrl.enable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }


     resetPassword(){
 //// check to confirm the username, email, telephone and password fields are filled

 
 if(this.mobilenumber.value==""){

 let alert = this.alertCtrl.create({

 title:"ATTENTION",
 subTitle:"Mobile Number field is empty",
 buttons: ['OK']
 });

 alert.present();
      
}
 else
 if(this.password.value==""){

 let alert = this.alertCtrl.create({

 title:"ATTENTION",
 subTitle:"Password field is empty",
 buttons: ['OK']
 });

 alert.present();
      
}
 else 
 {


var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

  let data = {
        password: this.password.value,
        clubid: this.clubid.value,
        mobilenumber: this.mobilenumber.value      
      };


 let loader = this.loadingCtrl.create({
    content: 'Processing please wait...',
  });

 loader.present().then(() => {
this.http.post('http://bet-ways.com/appdata/forgot.php',data, options)
.map(res => res.json())
.subscribe(res => {

 loader.dismiss()
 if(res=="Password successfull Changed"){
  let alert = this.alertCtrl.create({
    title:"CONGRATS",
    subTitle:(res),
    buttons: ['OK']
    });
   
    alert.present();
     this.navCtrl.setRoot(LoginPage);

 

}
else
{
 let alert = this.alertCtrl.create({
 title:"ERROR",
 subTitle:(res),
 buttons: ['OK']
 });

 alert.present();
  } 
});
});
 }

}


/*
  resetPassword(){

    let loading = this.loadingCtrl.create({
      content: 'Hold on...'
    });
  
    loading.present();

    this.auth.resetPasswordInit(this.forgotPasswordForm.value.email) 
    .then(
        response =>{
          this.loginError = 'Email with password reset instructions has been sent to ' +this.forgotPasswordForm.value.email;
          this.alert_title ='Successful';
          loading.dismiss();
          this.presentAlert();
        }
      ,e=>{

        switch(e.code){
          case 'auth/user-not-found':
          this.loginError = 'User with that email not found';
          this.alert_title = 'Oops!'
          loading.dismiss();
          this.presentAlert();
          break;
  
          case 'auth/invalid-email':
          this.loginError = 'Invalid email address';
          this.alert_title = 'Oops!'
          loading.dismiss();
          this.presentAlert();
          break;
        }
      })
    
  

  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: this.alert_title,
      subTitle:  this.loginError,
      buttons: ['Dismiss']
    });
    alert.present();
  } */
}
