import { AboutPage } from "./../about/about";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { ViewController } from "ionic-angular/navigation/view-controller";
import {
  InAppBrowser,
  InAppBrowserOptions
} from "@ionic-native/in-app-browser";

@IonicPage()
@Component({
  selector: "page-popover",
  templateUrl: "popover.html"
})
export class PopoverPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private inAppBrowser: InAppBrowser
  ) {}

  openWebpage(url: string) {
    const options: InAppBrowserOptions = {
      zoom: "no"
    };
    // Opening a URL and returning an InAppBrowserObject
    const browser = this.inAppBrowser.create(url, "_self", options);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad PopoverPage");
  }

 

  close() {
    this.viewCtrl.dismiss();
  }
}
