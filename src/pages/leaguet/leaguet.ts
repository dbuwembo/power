import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { LoadingController } from "ionic-angular/components/loading/loading-controller";
import { ItemsProvider } from "../../providers/webitems/webitems";

import 'rxjs/add/operator/map';
import {Http, Headers, RequestOptions}  from "@angular/http";
import {Storage} from '@ionic/storage';

@Component({
  selector: 'page-leaguet',
  templateUrl: 'leaguet.html'
})
export class LeaguetPage {
  
  mybody:any
  stands:any
  isLoading: boolean;
  connectionFailed = false;
  loggedInUser:any;
  loggedInNumber:any;
  loggedInEmail:any;
  newsTitle:any;
  constructor(
  	public navCtrl: NavController, 
    public navParams: NavParams,
  	private _itemsSvc: ItemsProvider,
    public loadingCtrl: LoadingController,
    private http: Http,
    private storage: Storage) {

  }


ionViewDidLoad() {
    this.refresh();
    this.storage.get('authenticatedUser').then(res=>{
      console.log(res)
        
        
     this.loggedInUser = res?res.name:''; 
     this.loggedInNumber = res?res.phonenumber:'';
     this.loggedInEmail = res?res.email:'';
     this.newsTitle = 'Accessed League Table';
      var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let data = {
        name: this.loggedInUser,
        data: this.newsTitle,
        mobile: this.loggedInNumber,
        clubid: 'power',
        type: 'TablePage'
      };
    this.http.post('http://bet-ways.com/appdata/audit.php',data, options)
    .map(res => res.json())
    .subscribe(res => {
     console.log(res); 

    });
      
    });
  }
    getTable() {

    this._itemsSvc.getTable().subscribe(
      res => {
        this.isLoading = false;
        this.mybody = res.nodes[0].node.Leagueyear;
        
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );
  }

      getStandings() {
        this.isLoading = true;
        let loader = this.loadingCtrl.create({
        spinner: 'hide',
        cssClass: 'my-loading-class',
        content: `
      <div class="custom-spinner-container">
        <div class="custom-spinner-box ">
           <img class="animated bounceIn" src="assets/imgs/icon.png" />
        </div>
        <div class="overlay"></div>
      </div>`,
    duration: 5000
    });
    loader.present();

    this._itemsSvc.getStandings().subscribe(
      res => {
        
        this.isLoading = false;
        this.stands = res.nodes;
        console.log(this.stands);
        loader.dismiss();

        
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );
  }
   
     refresh() {
    this.getTable();
    this.getStandings();
 

  }

}
