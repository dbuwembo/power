import { NewsPageModule } from "./news.module";
import { NewsDetailsPage } from "./../news-details/news-details";
import { ItemsProvider } from "../../providers/webitems/webitems";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { LoadingController } from "ionic-angular/components/loading/loading-controller";

@IonicPage()
@Component({
  selector: "page-news",
  templateUrl: "news.html"
})
export class NewsPage {
  news: any;
  isLoading: boolean;
  currentPage = 0;
  connectionFailed = false;
  newItems: any
  errorMessage: string;
  page = 0;
  toggled: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _itemsSvc: ItemsProvider,
    public loadingCtrl: LoadingController
  ) {}

  ionViewDidLoad() {
    this.getAllNews();
  }

  getAllNews() {
    this.isLoading = true;
    let loader = this.loadingCtrl.create({
    spinner: 'hide',
    cssClass: 'my-loading-class',
    content: `
      <div class="custom-spinner-container">
        <div class="custom-spinner-box ">
           <img class="animated bounceIn" src="assets/imgs/icon.png" />
        </div>
        <div class="overlay"></div>
      </div>`,
    duration: 5000
    });
    loader.present();
    this._itemsSvc.getAllNews(this.currentPage).subscribe(
      res => {
        this.isLoading = false;
        this.news = res.nodes;
        loader.dismiss();
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
        console.log(err.name);
        loader.dismiss();
      }
    );
  }

  openPage(data) {
    console.log(data);
    this.navCtrl.push(NewsDetailsPage, { details: data });
  }

  doRefresh(refresher) {
    this._itemsSvc.getAllNews(this.currentPage).subscribe(
      res => { 
        this.isLoading = false;
        this.news = res.nodes;
        refresher.complete();
      },
      err => {
        this.isLoading = false;
        refresher.complete();
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
        console.log(err.name);
      }
    );
  }

       //load all items if end is reached
    doInfinite(infiniteScroll) {
    this.page = this.page+1;
    setTimeout(() => {
      this._itemsSvc.getAllNews(this.page)
         .subscribe(
           res => {
             this.newItems = res.nodes;
             for(let i=0; i<this.newItems.length; i++) {
               this.news.push(this.newItems[i]);
             }
           },
           error =>  this.errorMessage = <any>error);

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1000);
  }



}
