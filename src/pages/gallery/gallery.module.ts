import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { GalleryPage } from "./gallery";

@NgModule({
  declarations: [
    //NewsPage,
  ],
  imports: [IonicPageModule.forChild(GalleryPage)]
})
export class GalleryPageModule {}
