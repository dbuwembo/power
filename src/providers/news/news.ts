import { AppSettings } from "./../../app/app.settings";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

@Injectable()
export class NewsProvider {
  constructor(public http: HttpClient) {}

  getAll(page): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/jsondataNews?page=` + page
    );
  }
    getAllSlides(page): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/jsondataSlides?page=` + page
    );
  }
}
