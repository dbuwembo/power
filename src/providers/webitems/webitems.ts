import { AppSettings } from "./../../app/app.settings";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ItemsProvider {
  constructor(public http: HttpClient) {}

 
    getSlides(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/slideJson` 
    );
  }
   getLatestResults(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/nextmatchJson` 
    );
  }

   getNextGame(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/nextgamesJson` 
    );
  }
  getAbout(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/aboutJson` 
    );
  }

  getTable(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/leaguenJson` 
    );
  }
   getLatestMatch(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/latestmatchJson` 
    );
  }
   getAllMatches(page): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/matchJson?page=` + page 
    );
  }

   getAllClubs(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/clubsJson` 
    );
  }

  getStandings(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/standJson` 
    );
  } 

  getAllTeam(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/teamJson` 
    );
  }

  getAllPTeam(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/teampJson` 
    );
  }

  getGallaries(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/galleryJson` 
    );
  }
  
  getAllNews(page): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/newsJson?page=` + page 
    );
  }
  getAllFixtures(page): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/fixturesJson?page=` + page 
    );
  }
  getAllVideos(page): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/videosJson?page=` + page 
    );
  }
  getNewsList(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/newslistJson` 
    );
  }
  getTrogphies(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/trophiesJson` 
    );
  }

  getUcupSingle(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/ucupsingleJson` 
    );
  }

  getSuperSingle(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/supsingleJson` 
    );
  }

  getPremierSingle(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/psingleJson` 
    );
  }


getSuperDouble(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/supdoubleJson` 
    );
  }

  getUcupDouble(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/ucupdoubleJson` 
    );
  }

  getPremierDouble(): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/pdoubleJson` 
    );
  }

  getPremierAll(page): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/pallJson?page=` + page 
    );
  }


getUcupAll(page): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/ucupallJson?page=` + page 
    );
  }


getSuperAll(page): Observable<any> {
    return this.http.get(
      `${AppSettings.API_ENDPOINT}/supallJson?page=` + page 
    );
  }

  

  
}
