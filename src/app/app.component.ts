import { Component, ViewChild  } from '@angular/core';
import {  Nav, Platform  } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { MatchesPage } from '../pages/matches/matches';
import { AboutPage } from '../pages/about/about';
import { LeaguetPage } from '../pages/leaguet/leaguet';

import { NewsPage } from '../pages/news/news';
import { TeamPage } from '../pages/team/team';
import { GalleryPage } from '../pages/gallery/gallery';

import {
  InAppBrowser,
  InAppBrowserOptions
} from "@ionic-native/in-app-browser";
import { PopoverController } from "ionic-angular";
import { LoginPage } from '../pages/login/login';
import { AuthProvider } from '../providers/auth/auth';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import {Storage} from "@ionic/storage";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  @ViewChild(Nav) nav : Nav;
  pages: Array<{title:string,  icon: string, desc:string , class:string, component:any}>;
  activePage: any; 
  authenticated_user : any;
  is_loading = false;
   loggedInUser: String;
  data:any

  constructor(public platform: Platform, public statusBar: StatusBar, splashScreen: SplashScreen, 
     private inAppBrowser: InAppBrowser,public storage: Storage,
     public popoverCtrl: PopoverController, public auth: AuthProvider, public loadingCtrl: LoadingController, ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // let status bar overlay webview

     // set status bar to white
      this.statusBar.backgroundColorByHexString('#0B578E');
      splashScreen.hide();
      


    });

    //this.authenticated_user.displayName=''


   this.pages = [  
              
             {title: 'Home',  icon: 'home', class: 'mynclass',component: TabsPage,desc:''},
             {title: 'Our Story',  icon: 'contact', class: 'mynclass', component: AboutPage, desc: 'Club history and bio information'},
             {title: 'Team', class: 'myfavv',  icon: 'star', component: TeamPage, desc: 'Club team and management'},
             {title: 'League Results', class: 'myfavv',  icon: 'star', component: MatchesPage, desc: 'Game updates'},
             {title: 'Gallaries', class: 'myfavv', icon: 'star', component: GalleryPage, desc: 'View club Gallaries'},

             
            
       ];


      this.storage.get('authenticatedUser').then(res=>{
      console.log(res)
        if ((res) === null)
         {
        console.log("logged in");
        this.rootPage = LoginPage;
         }
        else
        {
        console.log("already logged in");
        this.rootPage = TabsPage;
         }
     this.loggedInUser = res?res.name:''; 
     
    });


    
  }

 
  ionViewDidLoad() {
    console.log('ionViewDidLoad this page ');
  }


   openPage(page){

    //let nav = this.app.getComponent('nav');
  //nav.setRoot(page.component, {index: page.index});

 this.nav.setRoot(page.component);
    this.activePage = page;

  }

  public checkActivePage(page): boolean{
    return page === this.activePage;
  }

    openUrl(url: string) {
    const options: InAppBrowserOptions = {
      zoom: "no"
    };
    // Opening a URL and returning an InAppBrowserObject
    const browser = this.inAppBrowser.create(url, "_system", options);
  }



logout() {

  let loading = this.loadingCtrl.create({
    content: 'logging out ...'
  });

  loading.present();
  this.auth.signOut()
  this.nav.setRoot(LoginPage);
  loading.dismiss();
}
}
