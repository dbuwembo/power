import { PopoverPage } from "./../pages/popover/popover";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { NewsDetailsPage } from "./../pages/news-details/news-details";
import { MatchDetailsPage } from "./../pages/match-details/match-details";
import { HighlightsDetailsPage } from "./../pages/highlights-details/highlights-details";

import { MatchesPage } from "./../pages/matches/matches";
import { FixturesPage } from "./../pages/fixtures/fixtures";
import { SuperPage } from "./../pages/super/super";
import { HighlightsPage } from "./../pages/highlights/highlights";
import { NewsPage } from "./../pages/news/news";
import { TeamPage } from "./../pages/team/team";
import { GalleryPage } from "./../pages/gallery/gallery";

import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { MyApp } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from '@angular/http';

import { IonicImageViewerModule } from 'ionic-img-viewer';


import { IonicStorageModule } from "@ionic/storage";
import { AboutPage } from "../pages/about/about";
import { LeaguetPage } from "../pages/leaguet/leaguet";

import { HomePage } from "../pages/home/home"; 
import { TabsPage } from "../pages/tabs/tabs"; 
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { NewsProvider } from "../providers/news/news";
import { ItemsProvider } from "../providers/webitems/webitems";

import { SocialSharing } from "@ionic-native/social-sharing";
import { LoginPage } from "../pages/login/login";
import {Youtube} from "../pipes/youtube";



import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

// //import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestoreModule} from 'angularfire2/firestore'

import { AuthProvider } from '../providers/auth/auth';
import { ForgotPasswordPage } from "../pages/forgot-password/forgot-password";
import { RegisterPage } from "../pages/register/register";

export const firebaseConfig = {
apiKey: "AIzaSyBjMucsMa7NCLrWnUBd6abN9uivrAQ8g3w",
authDomain: "powerbbc-c47c2.firebaseapp.com",
databaseURL: "https://powerbbc-c47c2.firebaseio.com",
projectId: "powerbbc-c47c2",
storageBucket: "powerbbc-c47c2.appspot.com",
messagingSenderId: "911577064375"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
    NewsPage,
    HighlightsPage,
    Youtube,
    MatchesPage,
    NewsDetailsPage,
    HighlightsDetailsPage,
    SuperPage,
    FixturesPage,
    GalleryPage,
    MatchDetailsPage,
    LeaguetPage,
    TeamPage,
    PopoverPage,
    LoginPage,
    ForgotPasswordPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicImageViewerModule,
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    MatchDetailsPage,
    HighlightsPage,
    SuperPage,
    LeaguetPage,
    TeamPage,
    FixturesPage,
    HighlightsDetailsPage,
    HomePage,
    TabsPage,
    GalleryPage,
    NewsPage,
    MatchesPage,
    NewsDetailsPage,
    PopoverPage,
    LoginPage,
    ForgotPasswordPage,
    RegisterPage
 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    NewsProvider,
    ItemsProvider,
    SocialSharing,
    InAppBrowser,
    AngularFireAuth,
    AuthProvider
  ]
})
export class AppModule {}
